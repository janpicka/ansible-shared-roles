# Role `debian_repo`

Manage debian `sources.list` repositories.


## Description

Default `sources.list` will be removed and all configured repositories are
managed in `sources.list.d` via Ansible `apt_repository` module.

Configured repositories are:
- Release
- Release updates
- Security updates


## Variables

| Variable                      | Required | Default                            | Description                                                                        |
|-------------------------------|----------|------------------------------------|------------------------------------------------------------------------------------|
| debian_repo_mirror            | no       | http://httpredir.debian.org/debian | URL of the Debian repository mirror                                                |
| debian_repo_release           | no       | {{ansible_distribution_release}}   | Debian release                                                                     |
| debian_repo_components        | no       | [main]                             | List of release components (can include `main`, `contrib` and `non-free` elements) |
| debian_repo_include_backports | no       | false                              | Install or uninstall *backports* repositories                                      |
| debian_repo_include_sources   | no       | false                              | Install or uninstall `deb-src` repositories                                        |


## Examples

Use the role in main playbook to all Debian servers:

```yaml
- name: Common roles
  hosts: all
  roles:
    - role: debian_repo
      when: ansible_distribution == 'Debian'
      tags:
        - debian_repo
```

Use custom (local) repository mirror:

```yaml
debian_repo_mirror: 'http://ftp.cz.debian.org/debian/'
```
