# Role `btrfs`

Prepare host for btrfs support.


## Description

This role will install *btrfs* tools and *Linux kernel* from *Debian
backports*. Installation source can be optionally set to normal (Debian)
release.

It is suitable to set `debian_repo_include_backports` from `debian_repo` role
to `true`.


## Variables

| Variable                      | Required  | Default   | Description |
| ----------------------------- | --------- | --------- | ----------- |
| btrfs_install_from_backports  | no        | true      | Install Linux kernel and `btrfs-progs` from *backports* |
| btrfs_kernel_architecture     | no        | amd64     | Architecture of installed Linux kernel |


## Examples

Install btrfs support from Debian backports:

```yaml

debian_repo_include_backports: true
```
