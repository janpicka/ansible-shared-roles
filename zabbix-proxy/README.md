# Role `zabbix-proxy`

Install and configure Zabbix proxy server on Debian-like system.


## Description

This role will install Zabbix proxy with PostgreSQL database on Debian server.
The proxy is configured to *passive mode* with *Pre-Shared-Key* secret to
encrypt server-to-proxy connection.

It is needed to enable *pipelining* due to becoming a different users when
managing Postgres database.


## Variables

| Variable          | Required | Default | Description |
| ----------------- | -------- | ------- | ----------- |
| zabbix_proxy_name | no       | inventory_hostname | Name of the installed Zabbix proxy instance |
| zabbix_proxy_psk  | yes      |         | Pre-Shared Key to use to check incoming server connections; 32 to 512 characters long hex-string |
| zabbix_proxy_identity | no   | zabbix_proxy | Pre-Shared Key identity string |
|                   |          |         |  |
| zabbix_proxy_dbpass | yes    |         | Password for `zabbix_proxy` Postgres database; Must be set to a random string; Used only for 'localhost' Zabbix connections |


## Examples

Enable *pipelining* in `ansible.cfg`:

```ini
[ssh_connection]
pipelining = True
```


The simplest configuration (in vault-protected vars):

```yaml
zabbix_proxy_psk: '1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952'
zabbix_proxy_dbpass: '4ndSh3bu1ngDS74irw4y2H34v3n'
```

Set some nice names:

```yaml
zabbix_proxy_name: 'Remote Proxy'
zabbix_proxy_identity: 'remote_proxy'
```
