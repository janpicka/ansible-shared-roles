# Role `php-fpm`

Install `php-fpm` from *deb.sury.org* repository.


## Variables

| Variable        | Required  | Default                             | Description |
| --------------- | --------- | ----------------------------------- | ----------- |
| php_version     | no        | 7.3                                 | Desired PHP version; See `deb.sury.org` repository for currently supported versions  |
| php_extensions  | no        |                                     | List of PHP extensions to install |
|                 |           |                                     |  |
| php_log_dir     | no        | `/var/log/php{{ php_version }}-fpm` | Log directory for php-fpm pools |


## Examples

Install php-fpm 7.2 with some extensions:

```yaml
php_version: '7.2'
php_extensions:
  - curl
  - gd
  - json
  - mbstring
  - mysql
  - zip
```
