# Role `ssh`

Simple role to install and setup ssh server and client on a host.


## Description

The role will disable *root* login via ssh and hashing of known hosts file.


## Variables

| Variable              | Required  | Default     | Description |
| --------------------- | --------- | ----------- | ----------- |
| ssh_permit_passwords  | no        | true        | Enable/disable password authentication |
