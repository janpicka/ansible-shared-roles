# Role `rsnapshot/server`

Role to prepare a server for backups with `rsnapshot` tool, optionally with
snapshots via *btrfs subvolumes* and *snapshots*.


## Variables

| Variable                    | Required  | Default         | Description |
| --------------------------- | --------- | --------------- | ----------- |
| rsnapshot_backup_dir        | yes       |                 | Root directory for backups; It must be on *btrfs* file system if `rsnapshot_btrfs_backend` is `true` |
| rsnapshot_btrfs_backend     | no        | true            | Deploy rsnapshot with *btrfs snapshots* support |
|                             |           |                 |  |
| rsnapshot_unmanaged_backups | no        |                 | List of backups of hosts not managed via Ansible; See `rsnapshot/instance` role for details |
