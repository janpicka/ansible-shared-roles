# Role `mailserver`

Install and configure *Postfix* as a regular mail server with DKIM mail-signing
support.

By default, this mailserver is not set as a final destination for given
domains, just to relay mails for them. As no mail inbox is present on the
server in this case, *imap* is not set by default as well.


## Description

The role will install *MTA* *postfix*, *MUA* (BSD) *mailx* and *opendkim*
daemon and tools. If user authentication is enabled, *dovecot* is installed as
well.

Mailserver is set to listen on all interfaces and optionally to receive mails
from given networks (or hosts) and relay e-mails to given domains. All e-mails
are passed to *opendkim* to sign headers with DKIM signature.

Opendkim daemon configuration support signing several domains with given
selector. Ansible prints the key DNS record for the first domain on the end of
role execution (records for other domains are the same).

Dovecot is used *only for SASL* authentication method and none of the imap,
pop, etc. is neither installed nor configured.


## Variables

`mail_domains` and `mail_relay_domains` is not needed for a system e-mail
notification mailsender. Otherwise, if you want a regular mail server, at least
one of these domains list should be set.


| Variable                        | Required | Default                             | Description                                                                                                                                                                    |
|---------------------------------|----------|-------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| admin_mails                     | yes      |                                     | List of e-mail addresses to forward `root` user e-mails                                                                                                                        |
|                                 |          |                                     |                                                                                                                                                                                |
| fail2ban_install                | no       | true                                | Configure `fail2ban` (integration with `firewall` role)                                                                                                                        |
|                                 |          |                                     |                                                                                                                                                                                |
| mail_relayhost                  | no       |                                     | Send e-mails through this *mail relay*                                                                                                                                         |
| mail_relayhost_port             | no       | 465                                 | Relay host port to deliver e-mails; Useful in combination of `mail_relayhost_use_smtps`                                                                                        |
| mail_relayhost_use_smtps        | no       | true                                | By default, smtps/submissions on port 465 is used to deliver e-mails to relay host; Turning this to `false` will use given port and StartTLS                                   |
| mail_relayhost_credentials      | no       |                                     | A dictionary with login credentials for *mail relay*; E-mails would be relayed through *mail relay* after login if defined                                                     |
| .username                       | yes      |                                     | User name for mail relay login                                                                                                                                                 |
| .password                       | yes      |                                     | Password for mail relay login                                                                                                                                                  |
|                                 |          |                                     |                                                                                                                                                                                |
| mail_name                       | no       | `inventory_hostname`                | FQDN mail name                                                                                                                                                                 |
| mail_mynetworks                 | no       |                                     | Add extra permitted networks                                                                                                                                                   |
| mail_domains                    | no       |                                     | List of domains this server will *receive* e-mails (main MX server; `mail_delivery_enabled` must be set)                                                                       |
| mail_relay_domains              | no       |                                     | List of domains this server will *relay* e-mails for (backup MX server or sender-only server)                                                                                  |
|                                 |          |                                     |                                                                                                                                                                                |
| mail_dkim_enabled               | no       | false                               | Enable DKIM signing; Should be set to `true` when                                                                                                                              |
| mail_dkim_domains               | no       | `mail_domains`+`mail_relay_domains` | List of domains for DKIM signing (required when `mail_domains` and `mail_relay_domains` are empty)                                                                             |
| mail_dkim_selector              | no       | `inventory_hostname_short`          | DKIM selector (used in mail header and DNS record)                                                                                                                             |
|                                 |          |                                     |                                                                                                                                                                                |
| mail_tls_key                    | no       | snakeoil.key                        | Used TLS key                                                                                                                                                                   |
| mail_tls_cert                   | no       | snakeoil.crt                        | Used TLS certificate                                                                                                                                                           |
| mail_tls_ciphers                | no       | see description                     | List of TLS cipher suites for *imap* and *submission* protocol; default list includes `ECDHE-ECDSA`, `ECDHE-RSA` and `DHE-RSA` combinations, without `SHA1`                    |
| mail_tls1_enabled               | no       | false                               | Enable old *TLS 1.0* and *1.1* for *submission*/*s* server (you would need to update `mail_tls_ciphers` as well)                                                               |
|                                 |          |                                     |                                                                                                                                                                                |
| mail_submissions_enabled        | no       | false                               | Enable *submissions* (*submission* over *TLS* on port 465)                                                                                                                     |
| mail_starttls_enabled           | no       | false                               | Enable StartTLS protocol variants as well – *submission* on 587 and *imap* on 143 (by default, only implicit TLS protocols are used – *submissions* on 465 and *imaps* on 993) |
| mail_smtp_starttls_enabled      | no       | true                                | Enable StartTLS protocol on port 25 (which is expected by default, even with untrusted certificate)                                                                            |
|                                 |          |                                     |                                                                                                                                                                                |
| mail_auth_enabled               | no       | false                               | Enable auth authentication method for clients (over TLS)                                                                                                                       |
| mail_auth_type                  | no       | system                              | Choose auth users database backend (`system` for system users, `sql` for PostgreSQL virtual user database, `virtual` for dovecot *passwd-file* user database)                  |
| mail_delivery_enabled           | no       | false                               | Enable local mail delivery and *imap* access to mailboxes; supported only when `mail_auth_type` is `virtual`                                                                   |
|                                 |          |                                     |                                                                                                                                                                                |
| mail_auth_db_host               | no       | localhost                           | Database server name (for `sql` users backend)                                                                                                                                 |
| mail_auth_db_name               | no       | mails                               | Database name (for `sql` users backend)                                                                                                                                        |
| mail_auth_db_table              | no       | users                               | Users table name (for `sql` users backend; see `simple_pg_schema.sql`)                                                                                                         |
| mail_auth_db_user               | no       | dovecot                             | Database user (for `sql` users backend)                                                                                                                                        |
| mail_auth_db_password           | no       |                                     | Database user password (for `sql` users backend; required when `mail_auth_type` is `sql`                                                                                       |
|                                 |          |                                     |                                                                                                                                                                                |
| mail_limit_enabled              | no       | false                               | Limit SASL-authenticated sender to send given number of e-mails for given time period                                                                                          |
| mail_limit_count                | no       | 100                                 | Given number of e-mails limit                                                                                                                                                  |
| mail_limit_period               | no       | 86400                               | Given time period (in seconds)                                                                                                                                                 |
|                                 |          |                                     |                                                                                                                                                                                |
| mail_restrict_submission_sender | no       | false                               | Limit sender address to `mail_domains` and `mail_relay_domains` and also reject e-mails from privileged addresses like `root`, `postmaster`, and `abuse`                       |
| mail_auth_filter_enabled        | no       | false                               | Enable sender address (`From`) mapping based on auth login user                                                                                                                |
|                                 |          |                                     |                                                                                                                                                                                |
| mail_auth_users                 | no       |                                     | List of users for smtp authentication (will be created as nologin system users)                                                                                                |
| .name                           | yes      |                                     | Name of the system user                                                                                                                                                        |
| .password                       | yes      |                                     | User password (should be in hashed format)                                                                                                                                     |
| .allowed_emails                 | no       |                                     | List of allowed *e-mail addresses* to send e-mails *from*                                                                                                                      |
| .allowed_domains                | no       |                                     | List of allowed *e-mail domains* to send e-mails *from*                                                                                                                        |
|                                 |          |                                     |                                                                                                                                                                                |
| mail_message_size_limit         | no       | 10                                  | E-mail size limit in Megabytes                                                                                                                                                 |
| mail_mailbox_size_limit         | no       | 0                                   | Mailbox size limit in Megabytes (0 means no limit)                                                                                                                             |
| mail_delay_warning_time         | no       | 4h                                  | Time to generate "delayed mail" warnings (`0h` means disable this feature; used only when `mail_delivery_enabled`)                                                             |
|                                 |          |                                     |                                                                                                                                                                                |
| mail_virtual_base               | no       | /var/mail/virtual                   | Directory with virtual user mailboxes                                                                                                                                          |
|                                 |          |                                     |                                                                                                                                                                                |
| mail_virtual_users              | no       |                                     | List of virtual mailbox accounts with *submission* and *imap* access (used only when `mail_auth_type` is `virtual`)                                                            |
| .email                          | yes      |                                     | Primary e-mail (serve as a *username* as well)                                                                                                                                 |
| .password                       | yes      |                                     | User password (use `doveadm pw -s PBKDF2` to generate strong password in hashed format)                                                                                        |
| .aliases                        | no       |                                     | List of aliases for the account                                                                                                                                                |
| .forward_to                     | no       |                                     | If defined, forward all mails to specified e-mail address and do not deliver them localy                                                                                       |
| .allowed_emails                 | no       |                                     | List of allowed *e-mail addresses* to send e-mails *from* (on top of `email` and `aliases`)                                                                                    |
| .allowed_domains                | no       |                                     | List of allowed *e-mail domains* to send e-mails *from*                                                                                                                        |
|                                 |          |                                     |                                                                                                                                                                                |
| mail_virtual_groups             | no       |                                     | List of virtual e-mail groups (serve just for forwarding address; used only when `mail_auth_type` is `virtual`)                                                                |
| .email                          | yes      |                                     | Group primary e-mail                                                                                                                                                           |
| .aliases                        | no       |                                     | List of aliases for the group                                                                                                                                                  |
| .destinations                   | yes      |                                     | List of e-mails to deliver group e-mails to                                                                                                                                    |
|                                 |          |                                     |                                                                                                                                                                                |
| mail_instances                  | no       |                                     | List of postfix *smtpd* instances; Useful to configure different postfix parameters per different bind address                                                                 |
| .listen_address                 | yes      |                                     | Bind address for given instance (do not forget on *IPv6*)                                                                                                                      |
| .config                         | no       |                                     | List of postfix configuration options and values in form of dictionary (`option: value`); These parameters would be passes as `-o option=value` to the *smtpd* in `master.cf`  |


## Examples

Simple system e-mail sender:

```yaml
admin_mails:
  - 'yoda@jedi.council.net'
```


Specify DKIM parameters:

```yaml
mail_dkim_enabled: true
mail_dkim_domains:
  - 'example.org'
mail_dkim_selector: 'sender'
```


Integration with Let's Encrypt certificate:

```yaml
mail_tls_key: '/var/lib/letsencrypt/simp_le/default/key.pem'
mail_tls_cert: '/var/lib/letsencrypt/simp_le/default/fullchain.pem'
```


Common usage for regular server (mail sending only with system users):

```yaml
mail_dkim_enabled: true
mail_dkim_selector: 'sender'
mail_relay_domains:
  - example.org
  - kenobi.wan.obi

mail_submissions_enabled: true
mail_auth_enabled: true
mail_auth_users:
  - name: 'obiwan'
    password: '{{ "D4rthM4ulSuxx" | password_hash("sha512", "RandomSalt") }}'
  - name: 'quigon'
    password: '{{ "secret_vaulted_pass" | password_hash("sha512", "RandomSalt") }}'

mail_tls_key: '/var/lib/letsencrypt/simp_le/default/key.pem'
mail_tls_cert: '/var/lib/letsencrypt/simp_le/default/fullchain.pem'
```


Example of allowed e-mails mapping (`From` filter):

```yaml
...
mail_auth_filter_enabled: true

mail_auth_users:
  - name: 'obiwan'
    password: '...'
    allowed_emails:
      - obi@kenobi.wan
    allowed_domains:
      - jedi.org
  - name: 'quigon'
    password: '...'
    allowed_emails:
      - qui@jinn.gon
    allowed_domains:
      - jedi.org
  - name: 'maul'
    password: '...'
    allowed_emails:
      - maul@sith.com
```


Configure and create *virtual* mail users and enable mail delivery:

```yaml
mail_domains:
  - example.org

mail_dkim_enabled: true
mail_auth_enabled: true
mail_auth_type: 'virtual'
mail_delivery_enabled: true
mail_submissions_enabled: true

mail_virtual_users:
  - email: 'john.doe@example.org'
    password: '...'
    aliases:
      - 'john@example.org'
      - 'doe@example.org'
      - 'postmaster@example.org'
      - 'abuse@example.org'
      - 'root@example.org'

  - email: 'joe.down@example.org'
    password: '...'
    aliases:
      - 'joe@example.org'
```

Password for virtual users should be in form of hash `dovedm pw` generates (see
`doveadm-pw(1)`). Recommended commands/*schemas* are:

```
doveadm pw -s ARGON2ID
doveadm pw -s BLF-CRYPT -r 6
doveadm pw -s SHA512-CRYPT -r 5555
```


Example of *virtual* mail group:

```yaml
mail_virtual_groups:
  - email: 'boys@example.org'
    destinations:
      - 'john.doe@example.org'
      - 'joe.down@example.org'
      - 'mother@exempli-gratia.org'
```


And example of simple forwarding e-mail address:

```yaml
mail_virtual_users:
  - email: 'mother@example.org'
    password: '...'
    forward_to: 'mother@exempli-gratia.org'
```


Configure *Dovecot* to look for mail users in table `citizens` in PostgreSQL
database `catalog` running on server `coruscant.galaxy.net` with user name
`council` and secret password:

```yaml
...
mail_auth_enabled: true
mail_auth_type: 'sql'

mail_auth_db_host: 'coruscant.galaxy.net'
mail_auth_db_name: 'catalog'
mail_auth_db_table: 'citizens'
mail_auth_db_user: 'council'
mail_auth_db_password: 'MayThe4th'
```


Restrict sender addresses to "my domains" and prohibit privileged e-mail
addresses and also limit number of e-mails to 15 messages per hour:

```yaml
...
mail_restrict_submission_sender: true

mail_limit_enabled: true
mail_limit_count: 15
mail_limit_period: 3600
```


Lessen security for clients – enable TLS 1.0+ and weaker ciphers (OpenSSL `HIGH`
is not high enough for modern setup):

```yaml
mail_tls1_enabled: true
mail_tls_ciphers:
  - '!aNULL'
  - 'HIGH'
```


Configure multiple postfix *smtpd* instances:

```yaml
mail_instances:
  - listen_address: '192.0.2.1'
    config:
      myhostname: 'master.coruscant.galaxy.net'

  - listen_address: '2001:db8:10da::1'
    config:
      myhostname: 'master.coruscant.galaxy.net'

  - listen_address: '203.0.113.5'
    config:
      myhostname: 'backup.dagobah.galaxy.net'

  - listen_address: '2001:db8:10da::5'
    config:
      myhostname: 'backup.dagobah.galaxy.net'
```
