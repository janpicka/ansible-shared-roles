---- users.sql
--
-- Simple schema for `users` table intended only as a password database (no uid, gid and home is present)

CREATE TABLE users (
    id BIGSERIAL PRIMARY KEY,
    username VARCHAR(128) NOT NULL,
    domain VARCHAR(128) NOT NULL,
    password VARCHAR(256) NOT NULL,
    active BOOLEAN DEFAULT TRUE
);
