# Ansible shared-roles

This is a directory with Ansible roles. It is intended to used as a *git
submodule*.


## Add as a submodule

```bash
git submodule add git@gitlab.com:VojtechMyslivec/ansible-shared-roles.git shared-roles
```

`ansible.cfg`:
```ini
roles_path = roles:shared-roles
```
