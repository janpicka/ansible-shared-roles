# Role `nginx/proxy`

*nginx* reverse proxy virtual host.


## Description

This role will set nginx *vhost* as a reverse proxy to another web server
(upstream). It is possible to set multiple servers so the nginx will balance
traffic among multiple upstream.

`proxy` role depends on `webserver` and optionally on `letsencrypt`, if
*letsencrypt* certificate is used (`https_letsencrypt`).


## Variables

Role takes one variable `proxy_vhosts` representing list of *reverse proxy*
virtual hosts. Each items is a *dictionary* with following parameters:

| Variable             | Required | Default      | Description                                                                                                   |
|----------------------|----------|--------------|---------------------------------------------------------------------------------------------------------------|
| name                 | yes      |              | Name that serve as an identifier (for file name, upstream, etc.)                                              |
| enabled              | no       | true         | Enable the vhost in nginx config                                                                              |
| server_name          | yes      |              | Server name for nginx vhost                                                                                   |
| aliases              | no       |              | List of vhost aliases                                                                                         |
|                      |          |              |                                                                                                               |
| server_name_redirect | no       | true         | Redirect from `aliases` to `server_name`                                                                      |
| security_headers     | no       | true         | Include basic security headers                                                                                |
|                      |          |              |                                                                                                               |
| https_enabled        | no       | true         | Enable *https* proxy vhost                                                                                    |
| https_redirect       | no       | true         | *https only* mode for proxy vhost, redirect http to https with *hsts* header                                  |
| https_letsencrypt    | no       | true         | Use *letsencrypt* generated certificate and key (from `acme` role)                                            |
| https_cert           | no       | snakeoil.crt | Path to a TLS certificate/fullchain (**required** if `https_letsencrypt` is `false`)                          |
| https_key            | no       | snakeoil.key | Path to a TLS key (**required** if `https_letsencrypt` is `false`)                                            |
|                      |          |              |                                                                                                               |
| websocket_enabled    | no       | false        | Enable websocket support in *vhost context* (sets `Upgrade` header etc... )                                   |
| vhost_config         | no       |              | Extra config for *nginx* vhost                                                                                |
| nginx_config         | no       |              | Extra config for *nginx* (*http context* – i.e. for `map` or another `upstream` service)                      |
|                      |          |              |                                                                                                               |
| proxy_location       | no       | /            | Nginx *location uri* where proxy is applied (can include nginx `location` *modifier* and should end with `/`) |
| proxy_target         | no       |              | Remote *URI* target where request should be forwarded                                                         |
| proxy_redirect       | no       | true         | Configure redirect to `proxy_location` from any other *location*                                              |
| proxy_protocol       | no       | http         | Protocol to use to *upstream* servers                                                                         |
| proxy_servers        | yes      |              | List of *upstream* server in `host/ip` or `host/ip:port` format (at least one server is needed)               |
| proxy_timeout        | no       |              | If set, configure `proxy_read_timeout` parameter to that value                                                |
|                      |          |              |                                                                                                               |
| restricted           | no       | false        | HTTP Basic authentication would be required to access the vhost                                               |
| restricted_users     | yes      |              | List of users for HTTP Basic authentication                                                                   |
| .name                | yes      |              | Name of the HTTP Basic user                                                                                   |
| .password            | yes      |              | Password for the HTTP Basic user                                                                              |
| .state               | no       | present      | The user would be removed if set to `absent`                                                                  |


## Examples

Simple reverse proxy to a web server:

```yaml
proxy_vhost:
  - name: 'my_cool_app'
    server_name: 'application.example.org'
    proxy_servers:
      - 'my_cool_app.example.local'
```


Load balance the traffic accross 3 app servers:

```yaml
proxy_vhost:
  - name: 'my_cool_app'
    server_name: 'application.example.org'
    proxy_servers:
      - 'my_cool_app1.example.local'
      - 'my_cool_app2.example.local'
      - 'my_cool_app3.example.local'
```


Reverse proxy for https app server:

```yaml
proxy_vhost:
  - name: 'my_cool_app'
    server_name: 'application.example.org'
    proxy_servers:
      - 'my_cool_app.example.local:443'
    proxy_protocol: 'https'
```


Reverse proxy for app on some location:

```yaml
proxy_vhost:
  - name: 'my_cool_app'
    server_name: 'application.example.org'
    proxy_servers:
      - 'shared.example.local'
    proxy_location: '/app1/'
```


Require HTTP Basic authentication for the whole web:

```yaml
proxy_vhost:
  - name: 'secret_app'
    server_name: 'secret.example.org'
    proxy_servers:
      - 'secret_app1.example.local'
    restricted: true
    restricted_users:
      - name: 'neo'
        password: '1amth31'
      - name: 'morpheus'
        password: 'letm31nN3buchadnezz4r'
      - name: 'trinity'
        password: 'l33th7x0r'
```


Include some extra config for virtual host:

```yaml
proxy_vhost:
  - name: 'my_cool_app'
    server_name: 'application.example.org'
    proxy_servers:
      - 'my_cool_app1.example.local'
    vhost_config: |
      client_max_body_size 32m;
```


Include some extra nginx config: define `$loggable` variable and use it for
conditional logs (from
[nginx documentation](https://nginx.org/en/docs/http/ngx_http_log_module.html#access_log)):

```yaml
proxy_vhost:
  - name: 'my_cool_app'
    server_name: 'application.example.org'
    proxy_servers:
      - 'my_cool_app1.example.local'
    nginx_config: |
      map $status $loggable {
        ~^[23]  0;
        default 1;
      }
    vhost_config: |
      access_log /path/to/access.log combined if=$loggable;
```
