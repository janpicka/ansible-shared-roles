# Role `nginx/redirect`

Simple *nginx* vhost to redirect to somewhere else.


## Description

This role will set nginx *vhost* as a redirect to given *location* (`target`).

`redirect` role depends on `webserver` and optionally on `letsencrypt`, if
*letsencrypt* certificate is used (`https_letsencrypt`).


## Variables

Role takes one variable `redirect_vhosts` representing list of *redirect*
virtual hosts. Each items is a *dictionary* with following parameters:

| Variable             | Required | Default      | Description                                                                          |
|----------------------|----------|--------------|--------------------------------------------------------------------------------------|
| name                 | yes      |              | Name that serve as an identifier (for file name etc.)                                |
| enabled              | no       | true         | Enable the vhost in nginx config                                                     |
| server_name          | yes      |              | Server name for nginx vhost                                                          |
| aliases              | no       |              | List of vhost aliases                                                                |
|                      |          |              |                                                                                      |
| https_enabled        | no       | true         | Enable *https* proxy vhost                                                           |
| https_redirect       | no       | true         | *https only* mode for proxy vhost, redirect http to https with *hsts* header         |
| https_letsencrypt    | no       | true         | Use *letsencrypt* generated certificate and key (from `acme` role)                   |
| https_cert           | no       | snakeoil.crt | Path to a TLS certificate/fullchain (**required** if `https_letsencrypt` is `false`) |
| https_key            | no       | snakeoil.key | Path to a TLS key (**required** if `https_letsencrypt` is `false`)                   |
|                      |          |              |                                                                                      |
| vhost_config         | no       |              | Extra config for *nginx* vhost                                                       |
|                      |          |              |                                                                                      |
| redirect_target      | yes      |              | Target *location* of the redirect                                                    |
| redirect_code        | no       | 301          | *HTTP Status code* for redirection                                                   |


## Examples

Simple redirection with *letsencrypt* certificate, http-to-https redirect and *hsts* enabled:

```yaml
redirect_vhosts:
  - name: 'url'
    server_name: 'url.example.org'
    redirect_target: 'very-long.dns-name.example.org/hidden/labyrinth/start.md'
```

Simple redirection with defined certificate and key:

```yaml
redirect_vhosts:
  - name: 'url'
    server_name: 'url.example.org'
    https_letsencrypt: false
    https_cert: '/etc/ssl/private/url-example/cert.pem'
    https_key: '/etc/ssl/private/url-example/key.pem'
    redirect_target: 'very-long.dns-name.example.org/hidden/labyrinth/start.md'
```
