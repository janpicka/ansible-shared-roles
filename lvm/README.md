# Role `lvm`

Setup a *logical volume manager*.


## Description

This role will create one *volume group* across (optionaly) several physical
devices, create desired *logical volumes* and setup mount points.

If a newly created partition should replace existing location, manual data
migration is needed.


## Variables

| Variable          | Required | Default                       | Description                                                                                             |
|-------------------|----------|-------------------------------|---------------------------------------------------------------------------------------------------------|
| volume_group_name | no       | vg_`inventory_hostname_short` | Name of the volume group to create and manage                                                           |
|                   |          |                               |                                                                                                         |
| physical_volumes  | no       |                               | List of physical devices for configured volume group; No volume group would be created if not specified |
|                   |          |                               |                                                                                                         |
| logical_volumes   | no       |                               | List of logical volumes to create                                                                       |
| .name             | yes      |                               | Logical volume name                                                                                     |
| .size             | yes      |                               | Size of the logical volume in megabytes (units like `k`, `m` and `g` are allowed)                       |
| .filesystem       | no       | ext4                          | File system type                                                                                        |
| .filesystem_opts  | no       | "-m 1" for "ext4" filesystem  | Extra options for `mkfs`                                                                                |
| .mount            | no       |                               | Mount point for the logical volume                                                                      |
| .mount_opts       | no       |                               | Extra options for `mount`                                                                               |


## Examples

Create volume group on top of `/dev/sdb` and setup several partitions:

```yaml
physical_volumes:
  - '/dev/sbd'

logical_volumes:
  - name: 'log'
    size: '2g'
    mount: '/var/log'

  - name: 'www'
    size: '8g'
    mount: '/var/www'

  - name: 'dummy'
    size: '500m'
```
