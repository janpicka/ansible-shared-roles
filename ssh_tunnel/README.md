# Role `ssh-tunnel`

Role to setup *sshd* as a server for reverse ssh-tunnel.


## Description

The role will create restricted user accounts with public ssh keys. This keys
can be used only to forward remote port to the clients.

Desired port must be allowed in the firewall.


## Variables

| Name        | Required | Default | Description                                                                       |
|-------------|----------|---------|-----------------------------------------------------------------------------------|
| ssh_tunnels | no       |         | List of configured tunnels; Each item is a *dictionary* with following parameters |
| .user       | yes      |         | User name for the restricted account                                              |
| .ssh_key    | yes      |         | Public ssh key(s) as a string                                                     |
| .state      | no       | present | Set to `absent` to remove ssh-tunnel (system user)                                |


## Examples

Define reverse ssh tunnel for Omnia:

```yaml
ssh_tunnels:
  - user: 'omnia'
    ssh_key: 'ssh-ed25519 AAAAmY/PubL1c/55hk33= root@omnia'
```
