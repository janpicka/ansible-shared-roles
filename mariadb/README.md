# Role `mariadb`

Install, deploy basic configuration and setup simple backups of MariaDB.


## Backups

Simple backups with `mysqldump` are created on regular basis and there are
24 *hourly* dumps, 7 *daily* dumps and 4 *weekly* dumps kept.

Each series can be switched off by appropriate flag in variables.


## Variables

| Variable                  | Required  | Default           | Description |
| ------------------------- | --------- | ----------------- | ----------- |
| mariadb_bind_address      | no        | 127.0.0.1         | MariaDB server listen address |
|                           |           |                   |  |
| mariadb_slow_query_time   | no        | 2                 | Log queries longer than this number of seconds |
| mariadb_tuning            | no        |                   | Multiline string with MariaDB extra config |
|                           |           |                   |  |
| mariadb_backup            | no        |                   | Dictionary with MariDB backup parameters |
| .dir                      | no        | /var/backup/mysql | Directory to store hourly, daily and weekly backups |
| .hourly                   | no        | true              | Enable and keep hourly backups |
| .hourly_count             | no        | 24                | Keep that many hourly backups |
| .daily                    | no        | true              | Enable and keep daily backups |
| .daily_count              | no        | 7                 | Keep that many daily backups |
| .weekly                   | no        | true              | Enable and keep weekly backups |
| .weekly_count             | no        | 4                 | Keep that many weekly backups |
|                           |           |                   |  |
| mariadb_databases         | no        |                   | List of databases; Each item is a dictionary with following parameters |
| .name                     | yes       |                   | Name of a database to create |
| .username                 | no        | `name`            | Name of a database user; defaults to database name |
| .host                     | no        | %                 | The 'host' part of the MySQL username |
| .password                 | no        |                   | Password for database user |
|                           |           |                   |  |
| mariadb_extra_users       | no        |                   | List of databases; Each item is a dictionary with following parameters |
| .username                 | yes       |                   | Name of a database user |
| .host                     | no        | %                 | The 'host' part of the MySQL username |
| .password                 | no        |                   | Password for database user |
| .privs                    | no        |                   | List of database priviledges in `db.table:priv1,priv2` format |


## Examples

Create a database with user and password:

```yaml
mariadb_databases:
  - name: 'my_db'
    password: 'Sup3rS3cr3t'
```


Setup some basic MariaDB tuning:

```yaml
mariadb_tuning: |
  # Increase buffers
  innodb_buffer_pool_size = 2G
  innodb_buffer_pool_instances = 2
```


Listen on all interfaces and addresses:

```yaml
mariadb_bind_address: '*'
```
