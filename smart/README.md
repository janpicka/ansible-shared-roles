# Role `smart`

Install smartmontools and setup *S.M.A.R.T.* monitoring.


## Variables

| Variable              | Required  | Default       | Description |
| --------------------- | --------- | ------------- | ----------- |
| smartd_test_report    | no        | true          | Send e-mail test report on samrtd service start |
| smartd_config         | no        | see examples  | List of smartd config lines (see `smartd.conf(5)` for a format help |



## Examples

Default SMART config:

```yaml
smartd_config:
  - 'DEVICESCAN -d removable -n standby -m root -M exec /usr/share/smartmontools/smartd-runner'
```

Monitor 2 disks through megaraid hardware controller and run periodic tests:

```yaml
smartd_config:
    - '/dev/sda -d sat+megaraid,1 -a -s (S/../(07|14|21|28)/./02|L/../01/./02)'
    - '/dev/sda -d sat+megaraid,2 -a -s (S/../(08|15|22|29)/./02|L/../02/./02)'
```
