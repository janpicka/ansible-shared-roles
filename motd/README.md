# Role `motd`

Simple dynamic message-of-the-day for the Debian servers.


## Variables

| Variable    | Required | Default     | Description                                                                  |
| ----------- | -------- | ----------- | ---------------------------------------------------------------------------- |
| motd_banner | no       | see example | (Multiline) text which would be displayed in message of the day in red color |


## Examples

Default `motd_banner` value:

```yaml
motd_banner: |-
  ==================== ** W A R N I N G ** ====================

  This is Ansible-managed server!

  Do not make any local changes.

  ==================== ** W A R N I N G ** ====================
```
