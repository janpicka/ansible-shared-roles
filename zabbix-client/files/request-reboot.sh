#!/bin/bash
# The script will print 1 if system reboot is requested and 0 otherwise
# Suitable to monitor via zabbix

if [[ -f '/var/run/reboot-required' ]]; then
    echo 1
else
    echo 0
fi
