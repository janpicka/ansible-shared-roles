#!/bin/bash
# The script will print number of e-mails currently being processed (postfix queue)
# Suitable to monitor via zabbix

set -o pipefail

# print 0 if $5 is empty
postqueue -p \
    | awk 'END {print $5+0}'
